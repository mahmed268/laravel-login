<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function redirect()
    {
        $usertype = Auth::user()->role_id;
        $activetype = Auth::user()->activation_id;
        if ($usertype == '1' && $activetype == '2') {
            return view('dashboard');
        } else if ($usertype == '2' && $activetype == '2') {
            return view('dashboard');
        } else {
            abort(403, 'Your account is not Activated yet.');
        }
    }
}
